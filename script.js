// HW ADVANCHED JS
// 1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript 
// В: Прототипное наследование в джс основано на том, что каждий об'єкт може мати прототип і він успадковує властивості та методи від свого прототипу.
// 2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
// В: super() у конструкторі класу-нащадка, це виклик конструктора його батьківського класу.

class Employe { 
    constructor(name,age,salary) {
        this._name = name
        this._age = age
        this._salary = salary
    }

    get name() {
        return this._name
    }
    set name(NewName) {
        this._name = NewName
    }

    get age() {
        return this._age
    }
    set age(NewAge) {
        this._age = NewAge
    }

    get salary() {
        return this._salary
    }
    set name(Newsalary) {
        this._salary = Newsalary
    }
}

class Programer extends Employe {
    constructor(name, age, salary,langue) {
        super(name,age,salary)
        this._langue = langue
    }

    get salary() {
        return this._salary *3
    }


    get langue() {
        return this._langue;
    }
    set langue(NewLang) {
        this._langue = NewLang
    }
}

const programer1 = new Programer('John', 30, 50000, ['JavaScript', 'Python']);
const programer2 = new Programer('Alice', 25, 60000, ['Java', 'C++']);

// Виведення інформації у консоль
console.log('Programer 1:', programer1);
console.log('Programer 2:', programer2);
